package com.example.ahsan.myapplication;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.flyer.maker.poster.R;


import java.util.ArrayList;

public class FirstGridActivity extends AppCompatActivity implements BillingProcessor.IBillingHandler , NavigationView.OnNavigationItemSelectedListener{
    GridView gvCVs;

    ArrayList<Integer> images = new ArrayList<>();
    public  ViewPager viewPager;
    int tab_position = 0;
    int adapter_position = 0;

    int[] resumes = {R.drawable.label2,R.drawable.label1,R.drawable.label3,R.drawable.label4,R.drawable.label5,
            R.drawable.label6,R.drawable.label7,
            R.drawable.label8,R.drawable.label9,R.drawable.label10,R.drawable.label11,R.drawable.label12,
            R.drawable.label13,R.drawable.label14
            ,R.drawable.label15,R.drawable.label16,R.drawable.label17,R.drawable.label18,R.drawable.label19,
            R.drawable.label20,R.drawable.label21,R.drawable.label22,R.drawable.label23,R.drawable.label24,
            R.drawable.label25 ,R.drawable.label26,R.drawable.label27,R.drawable.label28,R.drawable.label29,
            R.drawable.label30,R.drawable.label31,R.drawable.label32,R.drawable.label33,R.drawable.label34,
            R.drawable.label35,R.drawable.label37,R.drawable.label38,R.drawable.label39,
            R.drawable.label40,R.drawable.label41,R.drawable.label42,R.drawable.label43,R.drawable.label44,
            R.drawable.label45,R.drawable.label46,R.drawable.label47,R.drawable.label48,R.drawable.label49,
            R.drawable.label50,R.drawable.label51,R.drawable.label52,R.drawable.label53,R.drawable.label54,
            R.drawable.label55,R.drawable.label56,R.drawable.label57,R.drawable.label58,R.drawable.label59,R.drawable.label60,R.drawable.label61};

    int[] resumes_large = {R.drawable.largelabel2,R.drawable.largelabel1,R.drawable.largelabel3,R.drawable.largelabel4,R.drawable.largelabel5,R.drawable.largelabel6,R.drawable.largelabel7,
            R.drawable.largelabel8,R.drawable.largelabel9,R.drawable.largelabel10,R.drawable.largelabel11,R.drawable.largelabel12,
            R.drawable.largelabel13,R.drawable.largelabel14
            ,R.drawable.largelabel15,R.drawable.largelabel16,R.drawable.largelabel17,R.drawable.largelabel18,R.drawable.largelabel19,
            R.drawable.largelabel20,R.drawable.largelabel21,R.drawable.largelabel22,R.drawable.largelabel23,R.drawable.largelabel24,
            R.drawable.largelabel25 ,R.drawable.largelabel26,R.drawable.largelabel27,R.drawable.largelabel28,R.drawable.largelabel29,
            R.drawable.largelabel30,R.drawable.largelabel31,R.drawable.largelabel32,R.drawable.largelabel33,R.drawable.largelabel34,
            R.drawable.largelabel35,R.drawable.largelabel37,R.drawable.largelabel38,R.drawable.largelabel39,
            R.drawable.largelabel40,R.drawable.largelabel41,R.drawable.largelabel42,R.drawable.largelabel43,R.drawable.largelabel44,
            R.drawable.largelabel45,R.drawable.largelabel46,R.drawable.largelabel47,R.drawable.largelabel48,R.drawable.largelabel49,
            R.drawable.largelabel50,R.drawable.largelabel51,R.drawable.largelabel52,R.drawable.largelabel53,R.drawable.largelabel54,
            R.drawable.largelabel55,R.drawable.largelabel56,R.drawable.largelabel57,R.drawable.largelabel58,R.drawable.largelabel59,
            R.drawable.largelabel60,R.drawable.largelabel61};
    BillingProcessor bp;
    ImageView drawer_btn;
    GridViewAdapter gvAdapter;
    private long mLastClickTime = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_grid);
         gvAdapter = new GridViewAdapter(FirstGridActivity.this,resumes);


        final DrawerLayout drawerLayout =  findViewById(R.id.drawerlayout);
        drawer_btn = findViewById(R.id.drawer_btn);
        drawer_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        bp = new BillingProcessor(FirstGridActivity.this, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkk4JRFUP3RTGHqrTuGE0TaFEeFvO0fUGpGVlZuJZdssnOplvfKPXfTeVte+TBfNpjo7nEP0vzp/85R3i2dWKUUC3qm86RkxEzG34zfOjR2L9+049mV50Oc6j+qmdbMKITpyNqt37JO5Bslmhyux7TcZnK1ojJgnPgyDLKS/1LvoNp3Z+fMdlksiq6jj9fRd473d4cTEVILaR4s4pCMj4DsX4amPXiHnA3w7CDqbOZDdFQvXxuMSd4seaEDm0G1Vey15KV/1S54VIXjJ+HQaxaoaXcu00yVGacRkWh9lswFH3ALADS4MhRi/BDRwjCyHTKyl2vHkSGGJjOtiBXvSnTwIDAQAB",this);
        gvCVs = (GridView)findViewById(R.id.gvCvs);
        gvCVs.setAdapter(gvAdapter);

        gvCVs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i>10 && !bp.isPurchased(getResources().getString(R.string.product_id))){
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    startActivity(new Intent(FirstGridActivity.this , PremiumActivity.class));
                    adapter_position =i;
                }
                else {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    startActivity(new Intent(FirstGridActivity.this, MainActivity.class).putExtra("images_array", resumes_large)
                            .putExtra("position", i));
                }

            }
        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }
    Dialog dialog;
    public void showDialog(){
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.buy_dialog, null);
        TextView pro =  view.findViewById(R.id.pro);
        TextView skip =  view.findViewById(R.id.notpro);
        dialog = new Dialog(FirstGridActivity.this);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(view);
        dialog.setCancelable(true);
        dialog.show();


        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();


            }
        });
        pro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FirstGridActivity.this , PremiumActivity.class));
                dialog.dismiss();


            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {

    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
         if (errorCode == 102 || errorCode == 103)
                bp.purchase((Activity) this, getResources().getString(R.string.product_id));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerlayout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        drawer.closeDrawer(GravityCompat.START);
        else
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        adapter_position = gvCVs.getFirstVisiblePosition();
        super.onPause();
    }

    @Override
    protected void onResume() {
        GridViewAdapter gvAdapter = new GridViewAdapter(FirstGridActivity.this,resumes);
        gvCVs.setAdapter(gvAdapter);
        gvCVs.setFastScrollEnabled(true);
        gvCVs.setSelection(adapter_position+2);

        super.onResume();
    }

    @Override
    public void onBillingInitialized() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.upgradetoPro) {
            if(!bp.isPurchased(getResources().getString(R.string.product_id)) )
                startActivity(new Intent(FirstGridActivity.this , PremiumActivity.class));
            else
                Toast.makeText(this, "You are already upgraded to Premium", Toast.LENGTH_SHORT).show();
        }


        if (id == R.id.rate_us) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri
                    .parse("market://details?id=" + getPackageName())));

        } if (id == R.id.privacy) {

            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri
                        .parse("http://www.contentarcadepublishing.com/apps-privacy-policy.php")));
            } catch (ActivityNotFoundException e) {
                Toast.makeText(FirstGridActivity.this, "You don't have Google Play installed", Toast.LENGTH_LONG).show();
            }


        }
        if (id == R.id.exit) {
            finish();
        }
        else if (id == R.id.more_apps) {
            Uri uri = Uri.parse("market://search?q=pub:" + "C.A Apps");
            Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
            try {
                startActivity(myAppLinkToMarket);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(FirstGridActivity.this, "You don't have Google Play installed", Toast.LENGTH_LONG).show();
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerlayout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }
}
