package com.example.ahsan.myapplication;

/**
 * Created by Content Arcade on 3/7/2018.
 */

        import android.app.Activity;
        import android.content.Context;
        import android.support.annotation.NonNull;
        import android.support.annotation.Nullable;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ArrayAdapter;
        import android.widget.BaseAdapter;
        import android.widget.ImageView;
        import android.widget.TextView;

        import com.anjlab.android.iab.v3.BillingProcessor;
        import com.anjlab.android.iab.v3.TransactionDetails;
        import com.bumptech.glide.Glide;
        import com.bumptech.glide.load.engine.DiskCacheStrategy;
        import com.flyer.maker.poster.R;



public class GridViewAdapter extends BaseAdapter  implements BillingProcessor.IBillingHandler{


    Context context;

     public  int[] images;
    BillingProcessor bp;


    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {

    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        if (errorCode == 102 || errorCode == 103)
            bp.purchase((Activity) context, context.getResources().getString(R.string.product_id));
    }

    @Override
    public void onBillingInitialized() {

    }

    static class ItemViewHolder {
        ImageView ivCv;
        ImageView lock;
    }

    public GridViewAdapter(Context context ,int[] images) {
        this.context=context;
        this.images = images;

        bp = new BillingProcessor(context, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkk4JRFUP3RTGHqrTuGE0TaFEeFvO0fUGpGVlZuJZdssnOplvfKPXfTeVte+TBfNpjo7nEP0vzp/85R3i2dWKUUC3qm86RkxEzG34zfOjR2L9+049mV50Oc6j+qmdbMKITpyNqt37JO5Bslmhyux7TcZnK1ojJgnPgyDLKS/1LvoNp3Z+fMdlksiq6jj9fRd473d4cTEVILaR4s4pCMj4DsX4amPXiHnA3w7CDqbOZDdFQvXxuMSd4seaEDm0G1Vey15KV/1S54VIXjJ+HQaxaoaXcu00yVGacRkWh9lswFH3ALADS4MhRi/BDRwjCyHTKyl2vHkSGGJjOtiBXvSnTwIDAQAB",this);
    }


    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int i) {
        return images[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public  int pos=0;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ItemViewHolder viewHolder;
        viewHolder = new ItemViewHolder();

        View view = convertView;
        if(convertView == null ||  viewHolder.ivCv==null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            view = inflater.inflate(R.layout.single_list_item, parent, false);
            viewHolder.ivCv = (ImageView) view.findViewById(R.id.ivCV);
            viewHolder.lock = (ImageView) view.findViewById(R.id.lockicon);
        }
        else {
            view = convertView;
        }


            int url = images[position];

            Glide.with(context)
                    .load(url)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(viewHolder.ivCv);

            if (position>10 && !bp.isPurchased(context.getResources().getString(R.string.product_id)))
                viewHolder.lock.setVisibility(View.VISIBLE);
            else
                viewHolder.lock.setVisibility(View.INVISIBLE);
            pos=position;



           // viewHolder.ivCv.setImageResource(images[position]);

           // viewHolder.score = (TextView) row.findViewById(R.id.score);
            return view;
    }
}
