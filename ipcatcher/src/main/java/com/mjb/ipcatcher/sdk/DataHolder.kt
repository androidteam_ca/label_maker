package com.mjb.ipcatcher.sdk

import com.github.nkzawa.socketio.client.Socket

object DataHolder {
    var fileName: String = "grammar"
    var appIcon: Int = 0
    var mSocket: Socket? = null
    var appName: String? = null
    var privacyUrl: String = "https://www.contentarcade.com/privacy"
}
