package com.example.ahsan.myapplication;

import android.graphics.Bitmap;

/**
 * Created by Content Arcade on 3/7/2018.
 */
public class ImageItem {
    private Bitmap image;
    private String id;
    private String index;

    public ImageItem(Bitmap image, String id, String index) {
        super();
        this.image = image;
        this.id = id;
        this.index=index;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }


    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getIndex() {
        return index;
    }
}
