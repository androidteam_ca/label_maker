package com.example.ahsan.myapplication;

/**
 * Created by Content Arcade on 3/6/2018.
 */

import java.io.*;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.*;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;

import com.microsoft.onedrivesdk.saver.*;
import com.flyer.maker.poster.R;


/**
 * Activity that shows how the OneDrive SDK can be used for file saving
 *
 * @author pnied
 */
public class SaverMain extends Activity {

    /**
     * The default file size
     */
    private static final int DEFAULT_FILE_SIZE_KB = 100;

    /**
     * Registered Application id for OneDrive {@see http://go.microsoft.com/fwlink/p/?LinkId=193157}
     */
    private static final String ONEDRIVE_APP_ID = "178110df-736b-428a-9e24-b620b016b163";

    /**
     * The onClickListener that will start the OneDrive Picker
     */
    String filenamee;
    private final OnClickListener mStartPickingListener = new OnClickListener() {
        @Override
        public void onClick(final View v) {
            final Activity activity = (Activity) v.getContext();
            activity.findViewById(R.id.result_table).setVisibility(View.INVISIBLE);

            filenamee = getIntent().getStringExtra("filename");


            // Create a file
            final File f = new File(Environment.getExternalStorageDirectory(),"/FlyerMaker/" +filenamee +".docx");
            Uri uri;
            if (Build.VERSION.SDK_INT >= 24) {
                uri = FileProvider.getUriForFile(SaverMain.this, "com.labelcreator.label.make.provider", f);
            }else{
                uri =  Uri.fromFile(f);
            }

            mSaver.startSaving(activity, filenamee+ ".docx", uri);

            grantUriPermission(getPackageName(), uri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

          /*  Intent intent = new Intent(getPackageName());
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivity(intent);*/
            // Start the saver


        }
    };




    /**
     * The OneDrive saver instance used by this activity
     */
    private ISaver mSaver;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saver_main);

        // Create the picker instance
        mSaver = Saver.createSaver(ONEDRIVE_APP_ID);

        // Add the start saving listener
        findViewById(R.id.startSaverButton).setOnClickListener(mStartPickingListener);
        findViewById(R.id.result_table).setVisibility(View.INVISIBLE);
        filenamee = getIntent().getStringExtra("filename");
        // Create a file
        final File f = new File(Environment.getExternalStorageDirectory(),"/FlyerMaker/" +filenamee +".docx");
        Uri uri;
        if (Build.VERSION.SDK_INT >= 24) {
            uri = FileProvider.getUriForFile(SaverMain.this, "com.labelcreator.label.make.provider", f);
        }
        else{
            uri =  Uri.fromFile(f);
        }

        grantUriPermission("com.microsoft.skydrive", uri,
                Intent.FLAG_GRANT_READ_URI_PERMISSION);

        mSaver.startSaving(SaverMain.this, filenamee + ".docx", uri);

    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        // Check that we were able to save the file on OneDrive
        final TextView overallResult = (TextView) findViewById(R.id.overall_result);
        final TextView errorResult = (TextView) findViewById(R.id.error_type_result);
        final TextView debugErrorResult = (TextView) findViewById(R.id.debug_error_result);

        try {
            mSaver.handleSave(requestCode, resultCode, data);
            overallResult.setText(getString(R.string.overall_result_success));
            errorResult.setText(getString(R.string.error_message_none));
            debugErrorResult.setText(getString(R.string.error_message_none));
            SaverMain.this.finish();
        } catch (final SaverException e) {

            /*final AlertDialog.Builder dialog = new AlertDialog.Builder(SaverMain.this);
            dialog.setMessage("Please Grant Storage permission to OneDrive App.\nYou can do this manually by" +
                    ": Settings>Applications>OneDrive>permissions>");
            dialog.setTitle("Permission Required!");

            final AlertDialog alertDialog = dialog.create();
            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    alertDialog.dismiss();
                    SaverMain.this.finish();

                }
            });
            dialog.show();*/

            overallResult.setText(getString(R.string.overall_result_failure));
            errorResult.setText(e.getErrorType().toString());
            debugErrorResult.setText(e.getDebugErrorInfo());
        }
        findViewById(R.id.result_table).setVisibility(View.VISIBLE);
    }

    /**
     * Creates an file on the SDCard
     * @param filename The name of the file to create
     * @param size The size in KB to make the file
     * @return The {@link File} object that was created
     */
}
