/*
 *  Copyright 2015 Erkan Molla
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.example.ahsan.myapplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {

    public static SharedPreferences sharedPreferences;

    public Preferences(String preference, Context context) {
        sharedPreferences = context.getSharedPreferences(preference, Context.MODE_PRIVATE);
    }
    public static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setDownloadStatus(Context context,String packName , Boolean b){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(packName , b);
        editor.commit();
    }
    public static boolean getDownloadStatus(Context context , String packName){
        return getSharedPreferences(context).getBoolean(packName, false);
    }

    public static void setHintsClaim(Context context, Boolean b){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("hintsClaim" , b);
        editor.commit();
    }
    public static boolean getHintsClaim(Context context ){
        return getSharedPreferences(context).getBoolean("hintsClaim", true);
    }
    public static void setHintStartTime(Context context, long b){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putLong("startTime" , b);
        editor.commit();
    }
    public static long getHintStartTime(Context context ){
        return getSharedPreferences(context).getLong("startTime", 12345);
    }

}
