package com.example.ahsan.myapplication;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by yasir on 10/22/2016.
 */
public class SingeltonPattern {

    private static SingeltonPattern singleton = new SingeltonPattern( );

    String recipieType;
    String minCalories;
    String maxCalorie;
    ArrayList<String> imgFromBucket;

    public ArrayList<String> getHearts_Bucket() {
        return hearts_Bucket;
    }

    public void setHearts_Bucket(ArrayList<String> hearts_Bucket) {
        this.hearts_Bucket = hearts_Bucket;
    }

    ArrayList<String> hearts_Bucket;

    public ArrayList<String> getHearts_local() {
        return hearts_local;
    }

    public void setHearts_local(ArrayList<String> hearts_local) {
        this.hearts_local = hearts_local;
    }

    ArrayList<String> hearts_local;

    public ArrayList<String> getHearts_cloud() {
        return hearts_cloud;
    }

    public void setHearts_cloud(ArrayList<String> hearts_cloud) {
        this.hearts_cloud = hearts_cloud;
    }

    ArrayList<String> hearts_cloud;
    public ArrayList<String> getIconic_Bucket() {
        return iconic_Bucket;
    }

    public void setIconic_Bucket(ArrayList<String> iconic_Bucket) {
        this.iconic_Bucket = iconic_Bucket;
    }

    ArrayList<String> iconic_Bucket;

    public ArrayList<String> getIconic_local() {
        return iconic_local;
    }

    public void setIconic_local(ArrayList<String> iconic_local) {
        this.iconic_local = iconic_local;
    }

    ArrayList<String> iconic_local;

    public ArrayList<String> getIconic_cloud() {
        return iconic_cloud;
    }

    public void setIconic_cloud(ArrayList<String> iconic_cloud) {
        this.iconic_cloud = iconic_cloud;
    }

    ArrayList<String> iconic_cloud;



    public ArrayList<String> getLifestyle_local() {
        return Lifestyle_local;
    }

    public void setLifestyle_local(ArrayList<String> lifestyle_local) {
        Lifestyle_local = lifestyle_local;
    }

    ArrayList<String> Lifestyle_local;

    public ArrayList<String> getLifestyle_cloud() {
        return Lifestyle_cloud;
    }

    public void setLifestyle_cloud(ArrayList<String> lifestyle_cloud) {
        Lifestyle_cloud = lifestyle_cloud;
    }

    ArrayList<String> Lifestyle_cloud;

    public ArrayList<String> getLifestyle_Bucket() {
        return Lifestyle_Bucket;
    }

    public void setLifestyle_Bucket(ArrayList<String> lifestyle_Bucket) {
        Lifestyle_Bucket = lifestyle_Bucket;
    }

    ArrayList<String> Lifestyle_Bucket;



    public ArrayList<String> getColorful_local() {
        return Colorful_local;
    }

    public void setColorful_local(ArrayList<String> colorful_local) {
        Colorful_local = colorful_local;
    }

    ArrayList<String> Colorful_local;

    public ArrayList<String> getColorful_cloud() {
        return Colorful_cloud;
    }

    public void setColorful_cloud(ArrayList<String> colorful_cloud) {
        Colorful_cloud = colorful_cloud;
    }

    ArrayList<String> Colorful_cloud;

    public ArrayList<String> getColorful_Bucket() {
        return Colorful_Bucket;
    }

    public void setColorful_Bucket(ArrayList<String> colorful_Bucket) {
        Colorful_Bucket = colorful_Bucket;
    }

    ArrayList<String> Colorful_Bucket;
    public ArrayList<String> getSports_local() {
        return Sports_local;
    }

    public void setSports_local(ArrayList<String> sports_local) {
        Sports_local = sports_local;
    }

    ArrayList<String> Sports_local;

    public ArrayList<String> getSports_cloud() {
        return Sports_cloud;
    }

    public void setSports_cloud(ArrayList<String> sports_cloud) {
        Sports_cloud = sports_cloud;
    }

    ArrayList<String> Sports_cloud;

    public ArrayList<String> getSports_Bucket() {
        return Sports_Bucket;
    }

    public void setSports_Bucket(ArrayList<String> sports_Bucket) {
        Sports_Bucket = sports_Bucket;
    }

    ArrayList<String> Sports_Bucket;








    public ArrayList<String> getAlpha_numerics_Bucket() {
        return Alpha_numerics_Bucket;
    }

    public void setAlpha_numerics_Bucket(ArrayList<String> alpha_numerics_Bucket) {
        Alpha_numerics_Bucket = alpha_numerics_Bucket;
    }

    ArrayList<String> Alpha_numerics_Bucket;
    public ArrayList<String> getAlpha_numerics_cloud() {
        return Alpha_numerics_cloud;
    }

    public void setAlpha_numerics_cloud(ArrayList<String> alpha_numerics_cloud) {
        Alpha_numerics_cloud = alpha_numerics_cloud;
    }

    ArrayList<String> Alpha_numerics_cloud;
    public ArrayList<String> getAlpha_numerics_local() {
        return Alpha_numerics_local;
    }

    public void setAlpha_numerics_local(ArrayList<String> alpha_numerics_local) {
        Alpha_numerics_local = alpha_numerics_local;
    }

    ArrayList<String> Alpha_numerics_local;


    public ArrayList<String> getFashion_Bucket() {
        return Fashion_Bucket;
    }

    public void setFashion_Bucket(ArrayList<String> fashion_Bucket) {
        Fashion_Bucket = fashion_Bucket;
    }

    ArrayList<String> Fashion_Bucket;
    public ArrayList<String> getFashion_cloud() {
        return Fashion_cloud;
    }

    public void setFashion_cloud(ArrayList<String> fashion_cloud) {
        Fashion_cloud = fashion_cloud;
    }

    ArrayList<String> Fashion_cloud;
    public ArrayList<String> getFashion_local() {
        return Fashion_local;
    }

    public void setFashion_local(ArrayList<String> fashion_local) {
        Fashion_local = fashion_local;
    }

    ArrayList<String> Fashion_local;


    public ArrayList<String> getHealth_Bucket() {
        return Health_Bucket;
    }

    public void setHealth_Bucket(ArrayList<String> health_Bucket) {
        Health_Bucket = health_Bucket;
    }

    ArrayList<String> Health_Bucket;
    public ArrayList<String> getHealth_cloud() {
        return Health_cloud;
    }

    public void setHealth_cloud(ArrayList<String> health_cloud) {
        Health_cloud = health_cloud;
    }

    ArrayList<String> Health_cloud;
    public ArrayList<String> getHealth_local() {
        return Health_local;
    }

    public void setHealth_local(ArrayList<String> health_local) {
        Health_local = health_local;
    }

    ArrayList<String> Health_local;
    public int getTab_position() {
        return tab_position;
    }

    public void setTab_position(int tab_position) {
        this.tab_position = tab_position;
    }

    int tab_position;

    public ArrayList<String> getBusiness_cloud() {
        return Business_cloud;
    }

    public void setBusiness_cloud(ArrayList<String> business_cloud) {
        Business_cloud = business_cloud;
    }

    ArrayList<String> Business_cloud;

    public ArrayList<String> getBusiness_bucket() {
        return Business_bucket;
    }

    public void setBusiness_bucket(ArrayList<String> business_bucket) {
        Business_bucket = business_bucket;
    }

    ArrayList<String> Business_bucket;

    public ArrayList<String> getBusiness_local() {
        return Business_local;
    }

    public void setBusiness_local(ArrayList<String> business_local) {
        Business_local = business_local;
    }

    ArrayList<String> Business_local;























    public ArrayList<String> getImgFromBucket() {
        return imgFromBucket;
    }

    public void setImgFromBucket(ArrayList<String> imgFromBucket) {
        this.imgFromBucket = imgFromBucket;
    }




    public ArrayList<HashMap<String, Object>> getMap() {
        return map;
    }

    public void setMap(ArrayList<HashMap<String, Object>> map) {
        this.map = map;
    }

    ArrayList<HashMap<String, Object>> map = new ArrayList<HashMap<String, Object>>();

    public String getLogoFolder() {
        return logoFolder;
    }

    public void setLogoFolder(String logoFolder) {
        this.logoFolder = logoFolder;
    }

    String logoFolder;

    public ArrayList<String> getLogoTypeArray() {
        return logoTypeArray;
    }

    public void setLogoTypeArray(ArrayList<String> logoTypeArray) {
        this.logoTypeArray = logoTypeArray;
    }
      ArrayList<String> logoTypeArray;





    public ArrayList<JSONObject> getCategoriesArray() {
        return categoriesArray;
    }

    public void setCategoriesArray(ArrayList<JSONObject> categoriesArray) {
        this.categoriesArray = categoriesArray;
    }

    ArrayList<JSONObject> categoriesArray;
    String ingredientType;

    String[] ingredientsArray;

    public String ingredientSeafood;
    public String ingredientTurky;
    public String ingredientChicken;
    public String ingredientPasta;
    public String ingredientFruit;
    public String ingredientStarch;
    public String ingredientBeef;
    public String ingredientBread;








    public String[] getMethodArray() {
        return methodArray;
    }

    public void setMethodArray(String[] methodArray) {
        this.methodArray = methodArray;
    }

    String[] methodArray;

    public String[] getIngredientsArray() {
        return ingredientsArray;
    }

    public void setIngredientsArray(String[] ingredientsArray) {
        this.ingredientsArray = ingredientsArray;
    }




    public boolean isAnimationshowflag() {
        return animationshowflag;
    }

    public void setAnimationshowflag(boolean animationshowflag) {
        this.animationshowflag = animationshowflag;
    }

    boolean animationshowflag;




    public String getIngredientType() {
        return ingredientType;
    }

    public void setIngredientType(String ingredientType) {
        this.ingredientType = ingredientType;
    }



    public String getMealType() {
        return mealType;
    }

    public void setMealType(String mealType) {
        this.mealType = mealType;
    }

    String mealType;










    int caloriesBudgetIndex;
    String[] values= {"All recipies",">100 calories", ">200 calories", ">300 calories", ">400 calories"

    };




    public String getRecipieType() {
        return recipieType;
    }

    public void setRecipieType(String recipieType) {
        this.recipieType = recipieType;
    }

    public String getMinCalories() {
        return minCalories;
    }

    public void setMinCalories(String minCalories) {
        this.minCalories = minCalories;
    }

    public String getMaxCalorie() {
        return maxCalorie;
    }

    public void setMaxCalorie(String maxCalorie) {
        this.maxCalorie = maxCalorie;
    }




    /* A private Constructor prevents any other
     * class from instantiating.
     */
    private SingeltonPattern() {

   // foodItems = new ArrayList<FoodItem>();

    }

    /* Static 'instance' method */
    public static SingeltonPattern getInstance( ) {
        return singleton;
    }

    /* Other methods protected by singleton-ness */
    protected static void demoMethod( ) {
        System.out.println("demoMethod for singleton");
    }



}
