package com.example.ahsan.myapplication;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.flyer.maker.poster.R;


public class ItemArrayAdapter extends PagerAdapter implements BillingProcessor.IBillingHandler {

    int[] images;
    BillingProcessor bp;
    public boolean subCategory;
    Context context;
    private LayoutInflater inflater;

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {

    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        if (errorCode == 102 || errorCode == 103)
            bp.purchase((Activity) context, context.getResources().getString(R.string.product_id));
    }

    @Override
    public void onBillingInitialized() {

    }


    static class ItemViewHolder {
        TextView name;
        TextView score;
        ImageView iv;
    }

    public ItemArrayAdapter(Context context, int rec, int[] images) {
        this.images = images;
        this.context = context;
        bp = new BillingProcessor(context, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkk4JRFUP3RTGHqrTuGE0TaFEeFvO0fUGpGVlZuJZdssnOplvfKPXfTeVte+TBfNpjo7nEP0vzp/85R3i2dWKUUC3qm86RkxEzG34zfOjR2L9+049mV50Oc6j+qmdbMKITpyNqt37JO5Bslmhyux7TcZnK1ojJgnPgyDLKS/1LvoNp3Z+fMdlksiq6jj9fRd473d4cTEVILaR4s4pCMj4DsX4amPXiHnA3w7CDqbOZDdFQvXxuMSd4seaEDm0G1Vey15KV/1S54VIXjJ+HQaxaoaXcu00yVGacRkWh9lswFH3ALADS4MhRi/BDRwjCyHTKyl2vHkSGGJjOtiBXvSnTwIDAQAB", this);

    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (RelativeLayout) object);
    }


    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.pager_item, container, false);
        final TouchImageView imageView = (TouchImageView) view.findViewById(R.id.imageView);
        final ImageView lock = (ImageView) view.findViewById(R.id.lockicon);
        imageView.setImageResource(images[position]);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        if (position > 10 && !bp.isPurchased(context.getResources().getString(R.string.product_id)))
            lock.setVisibility(View.VISIBLE);
        else
            lock.setVisibility(View.INVISIBLE);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position > 10 && !bp.isPurchased(context.getResources().getString(R.string.product_id)))
                    showDialog();
            }
        });
        container.addView(view);
        return view;
    }

    Dialog dialog;

    private void showDialog() {
        LayoutInflater inflater
            = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.buy_dialog, null);
        TextView pro = view.findViewById(R.id.pro);
        TextView skip = view.findViewById(R.id.notpro);
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(view);
        dialog.setCancelable(true);
        dialog.show();

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();


            }
        });
        pro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, PremiumActivity.class));
                dialog.dismiss();

            }
        });

    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //  container.removeView((LinearLayout) object);
        ((ViewPager) container).removeView((View) object);
    }
}
