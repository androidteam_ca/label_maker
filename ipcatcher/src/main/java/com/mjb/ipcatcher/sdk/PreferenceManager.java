package com.mjb.ipcatcher.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;


public class PreferenceManager {

    static final String PREFERENCE_NAME = "IP_CHECKER";
    static final String IS_PERMITTED = "IS_PERMITTED";
    static final String ASKED_ONCE = "ASKED_ONCE";

    private final SharedPreferences preference;

    public PreferenceManager(Context context) {
        this.preference = context.getSharedPreferences(PREFERENCE_NAME, Activity.MODE_PRIVATE);
    }

    public void setPermitted(boolean permitted) {
        SharedPreferences.Editor editor = preference.edit();
        editor.putBoolean(IS_PERMITTED, permitted);
        editor.apply();
        if (permitted) {
            setAskedOnce(true);
        }
    }

    public Boolean isPermitted() {
        return preference.getBoolean(IS_PERMITTED, false);
    }

    public void setAskedOnce(boolean asked) {
        SharedPreferences.Editor editor = preference.edit();
        editor.putBoolean(ASKED_ONCE, asked);
        editor.apply();
    }

    public Boolean isAskedOnce() {
        return preference.getBoolean(ASKED_ONCE, false);
    }

}
