package com.mjb.ipcatcher.sdk

import android.Manifest
import android.app.Activity
import android.content.Context
import android.util.Log

import com.droidnet.DroidNet
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.content.PermissionChecker
import com.mjb.ipcatcher.sdk.DataHolder.fileName


class IPChecker private constructor() {

    internal var sTasks: SocketTasks? = null
    private val mDroidNet: DroidNet
    private val preferenceManager: PreferenceManager

    private val permissionRC = 1111

    val isPermitted: Boolean
        get() = preferenceManager.isPermitted!!

    init {
        mDroidNet = DroidNet.getInstance()
        sTasks = SocketTasks(context)
        preferenceManager = PreferenceManager(context!!)
    }


    private fun connectSocket() {
        sTasks = SocketTasks(context)
        sTasks!!.connectSocket()
        DataHolder.mSocket = sTasks!!.mSocket
    }

    private fun disconnectSocket() {
        if (sTasks != null) {
            sTasks!!.disconnect()
        }
    }

    fun checkIpPermissionsWithMessage(activity: Activity, title: String, desc: String) {
        if(!preferenceManager.isAskedOnce) {
            if (preferenceManager.isPermitted) {
                setupDroid()
            } else {
                showPermissionDialog(activity, title, desc,null)
                mDroidNet.removeAllInternetConnectivityChangeListeners()
            }
        }
    }

    private fun setupDroid() {
        mDroidNet.addInternetConnectivityListener { isConnected ->
            if (isConnected) {
                if (preferenceManager.isPermitted) {
                    connectSocket()
                }
            }
        }
    }


    fun showPermissionDialog(activity: Activity, title: String, desc: String, tapListener: ViewDialog.OnTapActionListener?) {
        val alert = ViewDialog(activity, object : ViewDialog.OnTapActionListener {
            override fun onAllowed() {
              preferenceManager.isPermitted = true
              setupDroid()
              tapListener?.onAllowed()
//                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED
//                        && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//                    ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), permissionRC)
//                } else {
//                    preferenceManager.isPermitted = true
//                    setupDroid()
//                    tapListener?.onAllowed()
//                }
            }

            override fun onDenied() {
                preferenceManager.isPermitted = false
                disconnectSocket()
                mDroidNet.removeAllInternetConnectivityChangeListeners()
                tapListener?.onDenied()
            }

        })
        alert.showDialog(activity, title, desc)
    }

    fun onAppForeground() {
        if (preferenceManager.isPermitted!!) {
            DataHolder.mSocket?.let { socket ->

                if (!socket.connected()) {
                    connectSocket()

                    Log.d("sTaskstat", "connected")
                    //Toast.makeText(getApplicationContext(), "onForeground", Toast.LENGTH_SHORT).show();
                }

            }

            if (DataHolder.mSocket == null) {
                sTasks = SocketTasks(context!!.applicationContext)
                sTasks!!.connectSocket()
                DataHolder.mSocket = sTasks!!.mSocket


                Log.d("sTaskstat", "not connected")
                //Toast.makeText(getApplicationContext(), "onForeground", Toast.LENGTH_SHORT).show();
                //  sTasks.connectSocket();

            }
        }
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == permissionRC && grantResults[0] == PermissionChecker.PERMISSION_GRANTED) {
            preferenceManager.isPermitted = true
            setupDroid()
        }
    }

    private object LazyHolder {
        val INSTANCE = IPChecker()
    }

    companion object {
        private var context: Context? = null

        fun init(application: Context, appIcon: Int, appName: String) {
            DroidNet.init(application)
            context = application
            DataHolder.appName = appName
            DataHolder.fileName = fileName
            DataHolder.appIcon = appIcon
            DataHolder.privacyUrl = "https://www.contentarcade.com/privacy"
        }

        fun init(application: Context, appIcon: Int, appName: String, fileName: String, privacyUrl: String) {
            DroidNet.init(application)
            context = application
            DataHolder.appName = appName
            DataHolder.appIcon = appIcon
            DataHolder.fileName = fileName
            DataHolder.privacyUrl = privacyUrl
        }

        val instance: IPChecker
            get() = LazyHolder.INSTANCE

        fun getApplicationName(context: Context): String {
            val applicationInfo = context.applicationInfo
            val stringId = applicationInfo.labelRes
            return if (stringId == 0) applicationInfo.nonLocalizedLabel.toString() else context.getString(stringId)
        }
    }

}
