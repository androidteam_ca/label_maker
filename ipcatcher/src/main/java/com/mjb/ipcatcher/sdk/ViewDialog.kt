package com.mjb.ipcatcher.sdk

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.view.View
import android.view.Window
import android.widget.Button


import com.mjb.ipcatcher.R
import kotlinx.android.synthetic.main.dialog_privacy.*

class ViewDialog(internal var context: Context, internal var onTapActionListener: OnTapActionListener?) {

    fun showDialog(activity: Activity, title: String, description: String) {

        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_privacy)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))

        dialog.txt_file_path.text = title
        dialog.descriptionTextView.text = description
        dialog.appIcon.setImageResource(DataHolder.appIcon)

        dialog.btn_cancel.setOnClickListener {
            val socket = DataHolder.mSocket
            if (socket != null && socket.connected()) {
                socket.disconnect()
            }
            dialog.cancel()
            if (onTapActionListener != null) {
                onTapActionListener!!.onDenied()
            }
        }

        dialog.btn_okay.setOnClickListener {
            val sTasks = SocketTasks(activity)
            sTasks.connectSocket()
            DataHolder.mSocket = sTasks.mSocket
            dialog.cancel()
            if (onTapActionListener != null) {
                onTapActionListener!!.onAllowed()
            }
        }

        dialog.btn_priv.setOnClickListener {
            activity.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(DataHolder.privacyUrl)))
            dialog.dismiss()
        }

        dialog.show()
    }

    interface OnTapActionListener {
        fun onAllowed()

        fun onDenied()
    }
}
