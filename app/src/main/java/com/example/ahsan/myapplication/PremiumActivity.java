package com.example.ahsan.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.flyer.maker.poster.R;


public class PremiumActivity extends AppCompatActivity implements BillingProcessor.IBillingHandler {
    BillingProcessor bp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium);
        bp = new BillingProcessor(PremiumActivity.this, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmANuMYdbIOx/kv7jodpOHWFAUfaBods2zxwGvGEYtqyi4InFkr1tNVe7bZ1JPwZQauUStwHC8MYfV6D5keLRqlI3VQvNRiFtySliuFADND4r3j2XUZTuLwfR+Gb9uhiQKE1pia+7JpEOjucznXRl65sKkZS5z9ajS16bCGvpTaR4KfDkwYI+IBh2SsDA0MqPXWHfyIP8L10IIthNeRNOrqpfAUfNJtYsn/i3+7KBXTPMpIkkNwMv/v7M/KQ/6JBPwTIX6vYIwUHarQAEVXxgi/WSM7fVWAPBJR3zOxFR7ruNJO0SmXYrjMVzkJFxHjZNDKjaBSGvoTgEwFFs2eCU2QIDAQAB",this);
    }
    public void purchase(View view){
        bp.purchase((Activity) this,getResources().getString(R.string.product_id));
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        PremiumActivity.this.finish();

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        if (errorCode == 102 || errorCode == 103)
            bp.purchase((Activity) this, getResources().getString(R.string.product_id));
    }

    @Override
    public void onBillingInitialized() {

    }

}
