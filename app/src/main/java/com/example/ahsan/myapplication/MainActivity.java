package com.example.ahsan.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;

import com.droidnet.DroidListener;
import com.droidnet.DroidNet;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.flyer.maker.poster.R;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;




public class MainActivity extends AppCompatActivity  implements BillingProcessor.IBillingHandler
        , DroidListener {
    ListView lvCvs;
    BillingProcessor bp;
    JSONObject obj ;
    JSONArray m_jArry;
    JSONArray right_arry;
    JSONObject jo_inside ;
    JSONObject right_inside;
    int tab_position = 0;
    public  static ArrayList<HashMap<String, String>>  formList = new ArrayList<HashMap<String, String>>();
    HashMap<String, String> m_li;
    public static String packageName = "com.microsoft.office.word";
    public static Uri uri;
    public static ViewPager viewPager;
    public int[] images_array;

    public  static  String filename;
    private DroidNet mDroidNet;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        images_array= getIntent().getIntArrayExtra("images_array");
        position = getIntent().getIntExtra("position",0);
        viewPager = (ViewPager) findViewById(R.id.lvCvs);
        ItemArrayAdapter itemArrayAdapter = new ItemArrayAdapter(MainActivity.this,R.layout.simple_list_item,images_array);
        viewPager.setAdapter(itemArrayAdapter);
        viewPager.setCurrentItem(position);
        DroidNet.init(this);
        mDroidNet = DroidNet.getInstance();
        mDroidNet.addInternetConnectivityListener(this);

        bp = new BillingProcessor(MainActivity.this, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkk4JRFUP3RTGHqrTuGE0TaFEeFvO0fUGpGVlZuJZdssnOplvfKPXfTeVte+TBfNpjo7nEP0vzp/85R3i2dWKUUC3qm86RkxEzG34zfOjR2L9+049mV50Oc6j+qmdbMKITpyNqt37JO5Bslmhyux7TcZnK1ojJgnPgyDLKS/1LvoNp3Z+fMdlksiq6jj9fRd473d4cTEVILaR4s4pCMj4DsX4amPXiHnA3w7CDqbOZDdFQvXxuMSd4seaEDm0G1Vey15KV/1S54VIXjJ+HQaxaoaXcu00yVGacRkWh9lswFH3ALADS4MhRi/BDRwjCyHTKyl2vHkSGGJjOtiBXvSnTwIDAQAB",this);
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            } else
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }

        readJson();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDroidNet.removeInternetConnectivityChangeListener(this);
    }
    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (!isConnected) {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Internet is not available!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
       /* mRxDrive.disconnect();
        mSubscription.unsubscribe();*/
    }

    public static boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packagename, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public void readJson(){
        try {
            obj = new JSONObject(loadJSONFromAsset());
                m_jArry=obj.getJSONArray("pack1");
            for (int i = 0; i < m_jArry.length(); i++) {
                jo_inside = m_jArry.getJSONObject(i);
                Log.d("Details-->", jo_inside.getString("name"));
                String formula_value = jo_inside.getString("name");
                //Add your values in your `ArrayList` as below:
                m_li = new HashMap<String, String>();
                m_li.put("name", formula_value);
                formList.add(m_li);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("cvs.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
    public  static String  path ;
    boolean open=false;
    ProgressDialog progressDialog;

    public void downloadImage(String namee){
        if(isOnline(MainActivity.this)) {

             progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Downloading...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            File storagePath = new File(Environment.getExternalStorageDirectory(), "/FlyerMaker/");
            if(!storagePath.exists()) {
                storagePath.mkdirs();
            }
            final File myFile = new File(storagePath,namee +".docx");


            StorageReference storageRef = FirebaseStorage.getInstance().getReference();
            StorageReference pathReference = storageRef.child(namee+".docx");
            pathReference.getFile(myFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    progressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Successfully Downloaded" , Toast.LENGTH_LONG).show();
                    if(open)
                    open();
                    else
                        dialog_show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                       progressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Error : Bad Internet Connection" , Toast.LENGTH_LONG).show();
                    // Handle any errors
                }
            }).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    //calculating progress percentage
                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                    //displaying percentage in progress dialog
                    progressDialog.setMessage("Downloaded " + ((int) progress) + "%...");
                }
            });
        }
        else
            Toast.makeText(MainActivity.this, "No Internet Connection", Toast.LENGTH_LONG).show();
    }


    public void downloadShare(String namee){
        if(isOnline(MainActivity.this)) {

             progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Downloading...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            File storagePath = new File(Environment.getExternalStorageDirectory(), "/FlyerMaker/");
            if(!storagePath.exists()) {
                storagePath.mkdirs();
            }
            final File myFile = new File(storagePath,namee +".docx");


            StorageReference storageRef = FirebaseStorage.getInstance().getReference();
            StorageReference pathReference = storageRef.child(namee+".docx");
            pathReference.getFile(myFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    progressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Successfully Downloaded" , Toast.LENGTH_LONG).show();
                    final File f = new File(Environment.getExternalStorageDirectory(),
                            "/FlyerMaker/" + itemname + ".docx");
                    if (Build.VERSION.SDK_INT >= 24) {
                        filesuri = FileProvider.getUriForFile(MainActivity.this, "com.labelcreator.label.make.provider", f);
                    } else {
                        filesuri = Uri.fromFile(f);
                    }
                    Intent share = new Intent();
                    share.setAction(Intent.ACTION_SEND);
                    share.setType("application/msword");
                    share.putExtra(Intent.EXTRA_STREAM, filesuri);
                    share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(share);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                       progressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Error : Bad Internet Connection" , Toast.LENGTH_LONG).show();
                    // Handle any errors
                }
            }).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    //calculating progress percentage
                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                    //displaying percentage in progress dialog
                    progressDialog.setMessage("Downloaded " + ((int) progress) + "%...");
                }
            });
        }
        else
            Toast.makeText(MainActivity.this, "No Internet Connection", Toast.LENGTH_LONG).show();
    }


    Dialog dialog;
    public void showDialog(){
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.buy_dialog, null);
        TextView pro =  view.findViewById(R.id.pro);
        TextView skip =  view.findViewById(R.id.notpro);
        dialog = new Dialog(MainActivity.this);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(view);
        dialog.setCancelable(true);
        dialog.show();


        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();


            }
        });
        pro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this , PremiumActivity.class));
                dialog.dismiss();

            }
        });

    }

    private static  boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    private long mLastClickTime = 0;
    public void onBtnClick(View view){
        switch (view.getId()){

            case R.id.btnOpen:
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (viewPager.getCurrentItem()>10 && !bp.isPurchased(getResources().getString(R.string.product_id))){
                    startActivity(new Intent(MainActivity.this , PremiumActivity.class));
                }
                else
                onClick(MainActivity.this);

                break;
            case R.id.btnSync:
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (viewPager.getCurrentItem()>10 && !bp.isPurchased(getResources().getString(R.string.product_id))){
                    startActivity(new Intent(MainActivity.this , PremiumActivity.class));
                }
                else {
                    sync(MainActivity.this);
                }
            break;
           /* case R.id.btnPurchase:
                if(!bp.isPurchased("label_maker"))
                    bp.purchase((Activity) this,"label_maker");
                break;*/
           /* case R.id.btnOneDrive:
                break;
            case R.id.btnGoogleDrive:
                break;*/
        }
    }


    public String getItemname(){

        itemname=getResources().getResourceEntryName(images_array[viewPager.getCurrentItem()]);
        itemname=itemname.replace("large","");
        return itemname;
    }
    public void shareFile(View view){
        itemname=getItemname();
        final File f = new File(Environment.getExternalStorageDirectory(),
                "/FlyerMaker/" + itemname + ".docx");

        if (!bp.isPurchased(getResources().getString(R.string.product_id))
                && viewPager.getCurrentItem()>10)
            startActivity(new Intent(MainActivity.this,PremiumActivity.class));
        else {
            if (!f.exists()) {
                open = true;
                downloadShare(itemname);
            } else {

                if (Build.VERSION.SDK_INT >= 24) {
                    filesuri = FileProvider.getUriForFile(MainActivity.this, "com.labelcreator.label.make.provider", f);
                } else {
                    filesuri = Uri.fromFile(f);
                }
                Intent share = new Intent();
                share.setAction(Intent.ACTION_SEND);
                share.setType("application/msword");
                share.putExtra(Intent.EXTRA_STREAM, filesuri);
                share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(share);
            }
        }
    }



    PackageManager pm ;
     boolean isInstalled;
    public  String itemname ;
    public void onClick(Context context){
        pm = context.getPackageManager();
        isInstalled = isPackageInstalled(packageName, pm);
        position = viewPager.getCurrentItem();
        itemname=getResources().getResourceEntryName(images_array[viewPager.getCurrentItem()]);
            itemname=itemname.replace("large","");

        File file = new File(Environment.getExternalStorageDirectory(), "/FlyerMaker/" + itemname + ".docx");
        if(!file.exists()) {
            open=true;
            downloadImage(itemname);
        }
        else
            open();
    }
    Uri filesuri;
    public void dialog_show(){

        LayoutInflater layoutInflaterFilter = LayoutInflater.from(MainActivity.this);
        View v = layoutInflaterFilter.inflate(R.layout.cloud_option_layout, null);
        final AlertDialog.Builder filterBuilder = new AlertDialog.Builder(MainActivity.this);
        filterBuilder.setCancelable(true);

        ImageView btnOnedrive = (ImageView) v.findViewById(R.id.btnOneDrive);
        ImageView btnGoogleDrive = (ImageView) v.findViewById(R.id.btnGoogleDrive);
        ImageView btnClose = (ImageView) v.findViewById(R.id.btnClose);
        final AlertDialog FilterDialog = filterBuilder.create();
        FilterDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        final File f = new File(Environment.getExternalStorageDirectory(),
                "/FlyerMaker/" + itemname + ".docx");

        if (Build.VERSION.SDK_INT >= 24) {
            filesuri = FileProvider.getUriForFile(MainActivity.this, "com.labelcreator.label.make.provider", f);
        } else {
            filesuri = Uri.fromFile(f);
        }

        btnOnedrive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // filename = formList.get(viewPager.getCurrentItem()).get("name");
                try {

                    if (isOnline(MainActivity.this)) {

                        Intent share = new Intent();
                        share.setAction(Intent.ACTION_SEND);
                        share.setType("application/msword");
                        share.putExtra(Intent.EXTRA_STREAM, filesuri);
                        share.setPackage("com.microsoft.skydrive");
                        share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivityForResult(share, 99);

                    /* startActivity(new Intent(MainActivity.this, SaverMain.class)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                            .addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                            .putExtra("filename", itemname));*/
                    } else
                        Toast.makeText(MainActivity.this, "No Internet!", Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    Toast.makeText(MainActivity.this, "Please Install OneDrive App", Toast.LENGTH_SHORT).show();
                }
                FilterDialog.dismiss();


            }
        });

        btnGoogleDrive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isOnline(MainActivity.this)) {
                    try {
                        //filename = formList.get(viewPager.getCurrentItem()).get("name");

                        Intent share = new Intent();
                        share.setAction(Intent.ACTION_SEND);
                        share.setType("application/msword");
                        share.putExtra(Intent.EXTRA_STREAM, filesuri);
                        share.setPackage("com.google.android.apps.docs");
                        share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivity(share);
                /* startActivity(new Intent(MainActivity.this, CreateFileActivity.class)
                        .putExtra("filename", itemname));*/
                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this,
                                "Please install Google Drive App", Toast.LENGTH_SHORT).show();
                    }
                } else
                    Toast.makeText(MainActivity.this, "No Internet!", Toast.LENGTH_SHORT).show();

                FilterDialog.dismiss();

            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FilterDialog.cancel();
            }
        });
        FilterDialog.setView(v);
        FilterDialog.show();
    }

    @Override
    protected void onResume() {
        ItemArrayAdapter itemArrayAdapter = new ItemArrayAdapter(MainActivity.this,R.layout.simple_list_item,images_array);
        viewPager.setAdapter(itemArrayAdapter);
        viewPager.setCurrentItem(position);
        super.onResume();
    }

    @Override
    protected void onPause() {
        position = viewPager.getCurrentItem();
        super.onPause();
    }

    public void sync(Context context){
        pm = context.getPackageManager();
        isInstalled = isPackageInstalled(packageName, pm);
        int i = viewPager.getCurrentItem();
        itemname=getResources().getResourceEntryName(images_array[viewPager.getCurrentItem()]);
        itemname=itemname.replace("large","");
        File file = new File(Environment.getExternalStorageDirectory(), "/FlyerMaker/" + itemname + ".docx");
        if(!file.exists()) {
            open=false;
            downloadImage(itemname);
        }
        else
            dialog_show();


    }

    public void showMsWordDialog(final boolean isInstalled){

        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.msworddialog, null);
        dialog = new Dialog(MainActivity.this);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        ImageView done =  view.findViewById(R.id.btnInstall);
        ImageView icon = view.findViewById(R.id.icon);
        dialog.setContentView(view);
        dialog.setCancelable(true);
        dialog.show();

        if (!isInstalled) {
            done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
                    }
                    dialog.dismiss();

                }
            });
        }
        else {
            icon.setImageResource(R.drawable.popup_img);
            done.setImageResource(R.drawable.btndone);

            done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    File file = new File(Environment.getExternalStorageDirectory(), "/FlyerMaker/" +itemname + ".docx");
                    if(file.exists()){
                        if (Build.VERSION.SDK_INT < 23) {
                            if (file.exists()) {
                                Intent intent = new Intent();
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                                intent.setAction(Intent.ACTION_VIEW);
                                String type = "application/msword";
                                uri = Uri.fromFile(file);
                                intent.setDataAndType(uri, type);
                                startActivity(intent);

                            }
                        } else {
                            if (!isInstalled) {
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
                                }
                            } else {
                                if (file.exists()) {
                                    Intent intent = getPackageManager().getLaunchIntentForPackage(packageName);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                                    intent.setAction(Intent.ACTION_VIEW);
                                    String type = "application/msword";
                                    if (Build.VERSION.SDK_INT >= 24) {
                                        uri = FileProvider.getUriForFile(MainActivity.this, "com.labelcreator.label.make.provider", file);
                                    } else {
                                        uri = Uri.fromFile(file);
                                    }
                                    intent.setDataAndType(uri, type);
                                    startActivity(intent);
                                }
                            }
                        }
                        dialog.dismiss();

                    }
                }
            });



        }

    }





    public void open(){
        if (!isInstalled) {
          showMsWordDialog(false);
        }
        else {
           showMsWordDialog(true);
        }

    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
    }
    @Override
    public void onPurchaseHistoryRestored() {
    }
    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        if (errorCode == 102 || errorCode == 103)
            bp.purchase((Activity) this, getResources().getString(R.string.product_id));
    }
    @Override
    public void onBillingInitialized() {
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        for (Fragment fragment : getSupportFragmentManager().getFragments())
        {
            if (fragment != null)
            {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
        if (requestCode==99 && resultCode==RESULT_OK)
            Toast.makeText(this, "Successfully uploaded !", Toast.LENGTH_SHORT).show();
        super.onActivityResult(requestCode,resultCode,data);
    }
    }

